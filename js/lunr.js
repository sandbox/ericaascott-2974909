/**
 * @file
 */

(function ($) {
  Drupal.behaviors.lunr = {
    attach: function (context, settings) {
      $('#lunr-search-button').click(function () {
        var search_string = $('#lunr-search-text').val();
        window.location.replace('/search?text=' + search_string);
      });

      $(document).on("keypress", "#lunr-search-text", function (e) {
        if (e.which == 13) {
          var search_string = $('#lunr-search-text').val();
          window.location.replace('/search?text=' + search_string);
        }
      });

      if (window.location.pathname == '/search') {
        var arguments = window.location.search,
            arguments_split = arguments.split('&'),
            search_string = arguments_split[0].replace('?text=', ''),
            page = 1;

        if (arguments_split[1]) {
          page = parseInt(arguments_split[1].replace('page=', ''));
        }

        $('.search-query').html('"' + search_string + '"');

        var search_results = lunr_search_results(search_string);

        var use_pager = Drupal.settings.lunr.use_pager;
        var results_per_page = Drupal.settings.lunr.results_per_page;
        var page_num = 1;
        var count = 0;

        var output_results = '<div class="lunr-search"><ol class="search-results node-results">';
        for (var i = 0; i < search_results.length; i++) {
          output_results = output_results + lunr_create_result(search_results[i].ref, search_string, page_num);
          if (count < results_per_page) {
            count++;
          }
          else {
            count = 0;
            page_num = page_num + 1;
          }
        }
        output_results = output_results + '</ol>';
        if (page_num > 1 && use_pager) {
          output_results = output_results + lunr_create_pager(page_num, page);
        }
        output_results = output_results + '</div>';
        $('.search-body').html(output_results);

        if (page_num > 1) {
          $('.search-result').addClass('hidden');
          $('.search-result.page-' + page).removeClass('hidden');
        }
      }

      function lunr_search_results(search_string) {
        var documents = Drupal.settings.lunr.documents;

        var idx = lunr(function () {
          this.ref('results');
          this.field('results');

          documents.forEach(function (doc) {
            this.add(doc);
          }, this);
        });

        return idx.search(search_string);
      }

      function lunr_create_result(result, search_string, page_num) {
        var parts = result.split("///");
        var snippet = lunr_get_snippet(parts[1], search_string);
        var output = '<li class="search-result page-' + page_num + '"><h3 class="title"><a href="' + parts[2] + '">' + parts[0] + '</a></h3><div class="search-snippet-info"><p class="search-snippet">' + snippet + '</p></div></li>';
        return output;
      }

      function lunr_create_pager(num_pages, curr_page) {
        var start = 1;
        var end = 10;

        if ((curr_page - 4) > start) {
          start = curr_page - 4;
        }

        if (start != 1 && (start + 9) <= num_pages) {
          end = start + 9;
        }
        else if ((start + 9) > num_pages) {
          end = num_pages;

          if ((num_pages - 9) >= 1) {
            start = num_pages - 9;
          }
        }

        var output = '<ul class="search-pager pager">';
        if (curr_page > 1) {
          output = output + '<li class="pager-first first"><a href="/search?text=' + search_string + '&page=1">« first</a></li><li class="pager-previous"><a href="/search?text=' + search_string + '&page=' + (curr_page - 1) + '">‹ previous</a></li>';
        }
        if (start > 1) {
          output = output + '<li class="pager-ellipsis">…</li>';
        }
        for (i = start; i < end; i++) {
          if (i == curr_page) {
            output = output + '<li class="pager-current">' + i + '</li>';
          }
          else {
            output = output + '<li class="pager-item"><a href="/search?text=' + search_string + '&page=' + i + '">' + i + '</a></li>';
          }
        }
        if (end < num_pages) {
          output = output + '<li class="pager-ellipsis">…</li>';
        }
        if (curr_page != (num_pages - 1)) {
          output = output + '<li class="pager-next"><a href="/search?text=' + search_string + '&page=' + (curr_page + 1) + '">next ›</a></li><li class="pager-last last"><a href="/search?text=' + search_string + '&page=' + (num_pages - 1) + '">last »</a></li></ul>';
        }
        return output;
      }

      function lunr_get_snippet(body, key) {
        var indexes = {};
        if (!body) {
          return '';
        }
        indexes[0] = body.indexOf(key);
        if (indexes[0] == -1) {
          return body.substring(0, 256) + '...';
        }

        var string_length = key.length;
        var count = 1;
        var found = body.indexOf(key, indexes[0] + string_length);
        while (found != -1) {
          indexes[count] = found;
          found = body.indexOf(key, found + string_length);
          count = count + 1;
        }

        var snippet = '';
        if (count == 1) {
          snippet = snippet + ' ... ' + body.substring(indexes[0] - 123, indexes[0] + 123);
        }
        else {
          for (var i = 0; i < count; i++) {
            if (snippet.length < 256 - 105) {
              snippet = snippet + ' ... ' + body.substring(indexes[i] - 50, indexes[i] + 50);
            }
            else {
              break;
            }
          }
        }
        snippet = snippet + ' ... ';
        snippet = snippet.replace(new RegExp(key, 'g'), '<b>' + key + '</b>');

        return snippet;
      }
    }
  };
}(jQuery));
