*******************************************************************************

Lunr - https://lunrjs.com/

Description:
-------------------------------------------------------------------------------

An integration between Drupal and Lunr for a simple, client-side site search.

Installation & Use:
-------------------------------------------------------------------------------

1.  This module does not work with the Drupal Search module and it will be
    disabled when it is enabled. This means it does not integrate with any
    other search modules that require Search.
2.  Enable module in module list located at administer > structure > modules.
3.  Add the Lunr Search block to site at administer > structure > blocks.
4.  Navigate to administer > config > search > lunr > settings and configure
    your settings such as content types included and other options.
5.  Integrate Search Exclude NID by downloading the module but not enabling it
    (https://www.drupal.org/project/search_exclude_nid).
6.  Navigate to administer > config > search > lunr to re-index the site.
7.  Navigate to /search?text=SEARCH-TERM to view results.

Hints & Tips:
-------------------------------------------------------------------------------

1.  The Search Block HTML must include <input id="lunr-search-text" type="text">
    and <input type="submit" id="lunr-search-button"> for it to function.
2.  The Customize Page Header must include <div class="search-query"></div> if
    you would like the search string to be displayed in the header.
3.  Paths built by the Page Manager module will be automatically included. The
    exclusions field can be used to exclude any of these paths.

Author:
-------------------------------------------------------------------------------
Erica Scott <erica@appnovation.com>
https://www.drupal.org/u/ericaascott
